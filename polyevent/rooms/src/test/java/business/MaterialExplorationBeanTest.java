package business;

import arquillian.AbstractRoomTest;
import cucumber.api.java.it.Ma;
import fr.unice.polytech.isa.teamk.MaterialExploration;
import fr.unice.polytech.isa.teamk.entities.Material;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.COMMIT)
public class MaterialExplorationBeanTest extends AbstractRoomTest {

    @EJB
    private MaterialExploration materialExploration;

    @Test
    public void getAllMaterialsTest() {
        List<Material> materials = this.materialExploration.getAllMaterials();

        assertFalse(materials.size() > 0);
    }

    @Test
    public void getMaterialByTypeTest() {
        String type = "type";

        List<Material> materials = this.materialExploration.getMaterialByType(type);

        assertFalse(materials.size() > 0);
    }

}

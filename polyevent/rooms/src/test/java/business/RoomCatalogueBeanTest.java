package business;

import arquillian.AbstractRoomTest;
import fr.unice.polytech.isa.teamk.RoomCatalogueExploration;
import fr.unice.polytech.isa.teamk.RoomCatalogueModifier;
import fr.unice.polytech.isa.teamk.RoomTypeProvider;
import fr.unice.polytech.isa.teamk.entities.Room;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.COMMIT)
public class RoomCatalogueBeanTest extends AbstractRoomTest{

    @EJB
    private RoomCatalogueExploration catalogueBean;

    @EJB
    private RoomTypeProvider provider;

    @EJB
    private RoomCatalogueModifier modifier;

    @Test
    public void getAllRoomsTest() {
        List<Room> list = catalogueBean.getAllRooms();

        assertFalse(list.size() > 0);
    }

    @Test
    public void getRoomByIDTest() {
        String id = "ID";

        Room res = this.catalogueBean.getRoomByID(id);

        assertEquals(res, new Room());
    }

    @Test
    public void getRoomByTypeTest() {
        String type = "wanted";

        List<Room> roomList = this.catalogueBean.getRoomByType(type);

        assertFalse(roomList.size() > 0);
    }

    @Test
    public void getAllRoomTypeTest() {
        List<String> types = this.provider.getAllRoomType();

        assertFalse(types.size() > 0);
    }

    @Test
    public void addRoomTest() {
        assertFalse(this.modifier.addRoom(new Room()));
    }

    @Test
    public void delRoomByID() {
        String id = "ID";

        assertFalse(this.modifier.delRoomByID(id));
    }
}

package business;


import arquillian.AbstractRoomTest;
import fr.unice.polytech.isa.teamk.RoomDispenser;
import fr.unice.polytech.isa.teamk.entities.Event;
import fr.unice.polytech.isa.teamk.entities.Room;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import java.util.Date;

import static org.junit.Assert.assertFalse;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.COMMIT)
public class RoomDispenserBeanTest extends AbstractRoomTest{

    @EJB
    private RoomDispenser dispenser;

    @Test
    public void attributeRoomTest() {
        assertFalse(this.dispenser.attributeRoom(new Room(), new Event(), new Date(), new Date()));
    }

}

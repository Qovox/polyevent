package arquillian;

import fr.unice.polytech.isa.teamk.*;
import fr.unice.polytech.isa.teamk.components.MaterialExplorationBean;
import fr.unice.polytech.isa.teamk.components.RoomCatalogueBean;
import fr.unice.polytech.isa.teamk.components.RoomDispenserBean;
import fr.unice.polytech.isa.teamk.entities.Room;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.ClassLoaderAsset;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;

public abstract class AbstractRoomTest {

    @Deployment
    public static WebArchive createDeployment() {
        // Building a Web ARchive (WAR) containing the following elements:
        return ShrinkWrap.create(WebArchive.class)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addPackage(Room.class.getPackage())
                .addPackage(RoomCatalogueBean.class.getPackage())
                .addPackage(RoomDispenserBean.class.getPackage())
                .addPackage(MaterialExploration.class.getPackage())
                .addPackage(RoomCatalogueExploration.class.getPackage())
                .addPackage(RoomCatalogueModifier.class.getPackage())
                .addPackage(RoomDispenser.class.getPackage())
                .addPackage(RoomTypeProvider.class.getPackage())
                .addPackage(MaterialExplorationBean.class.getPackage())
                // Persistence file
                .addAsManifestResource(new ClassLoaderAsset("META-INF/persistence.xml"), "persistence.xml");
    }

}

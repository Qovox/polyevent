package fr.unice.polytech.isa.teamk.components;


import fr.unice.polytech.isa.teamk.MaterialExploration;
import fr.unice.polytech.isa.teamk.entities.Material;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class MaterialExplorationBean implements MaterialExploration{

    public MaterialExplorationBean(){

    }

    @Override
    public List<Material> getAllMaterials() {
        return new ArrayList<>();
    }

    @Override
    public List<Material> getMaterialByType(String materialType) {
        return new ArrayList<>();
    }
}

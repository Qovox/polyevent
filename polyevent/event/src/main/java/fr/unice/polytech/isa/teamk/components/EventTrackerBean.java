package fr.unice.polytech.isa.teamk.components;

import fr.unice.polytech.isa.teamk.Tracker;
import fr.unice.polytech.isa.teamk.entities.Event;
import fr.unice.polytech.isa.teamk.entities.EventStatus;
import fr.unice.polytech.isa.teamk.exceptions.UnknownEventException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.logging.Logger;

@Stateless
public class EventTrackerBean implements Tracker {

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    @PersistenceContext
    private EntityManager manager;

    @Override
    public EventStatus status(String eventId) throws UnknownEventException {
        Event event = manager.find(Event.class, Integer.parseInt(eventId));
        if (event == null)
            throw new UnknownEventException(eventId);
        return event.getStatus();
    }

}

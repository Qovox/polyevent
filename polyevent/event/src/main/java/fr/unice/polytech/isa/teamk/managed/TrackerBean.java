package fr.unice.polytech.isa.teamk.managed;

import fr.unice.polytech.isa.teamk.EventFinder;
import fr.unice.polytech.isa.teamk.entities.Event;
import fr.unice.polytech.isa.teamk.entities.Provider;
import fr.unice.polytech.isa.teamk.exceptions.UnknownEventException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

@ManagedBean
public class TrackerBean {

    private static final Logger log = Logger.getLogger(TrackerBean.class.getName());
    @EJB
    private EventFinder eventFinder;

    //    @ManagedProperty("#{param.eventName}")  // Will be automatically injected from the GET parameter
    private String eventName;

    public String getEventName() {
        return event.getName();
    }

    @ManagedProperty("#{param.eventId}")  // Will be automatically injected from the GET parameter
    private String eventId;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    private Event event;

    private Set<Provider> providerSet;

    public Set<Provider> getProviderSet() {
        return providerSet;
    }

    public void setProviderSet(Set<Provider> providerSet) {
        this.providerSet = providerSet;
    }

    @PostConstruct
    private void initializeEvent() {

        try {
            event = eventFinder.searchEventById(eventId);

            providerSet = event.getProviders();

            log.log(Level.INFO, "PROVIDESR --- " + event.getProviders());

        } catch (UnknownEventException e) {
            log.log(Level.INFO, "Unknown eventId", e);
            e.printStackTrace();
        }


    }

    public String getStatus() {
//        if(eventId == null) { return "No eventId given!"; }
//        try {
//            return tracker.status(eventId).name();
//        } catch (UnknownEventException ueid) {
//            log.log(Level.INFO, "Unknown eventId", ueid);
//            return "Unknown Order [" + eventId + "]";
//        }

        return event.getStatus().name();
    }

//    public String getProviders(){
//
//        return  providerSet.toString();
//
//    }

}

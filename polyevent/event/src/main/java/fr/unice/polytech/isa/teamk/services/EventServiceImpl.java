package fr.unice.polytech.isa.teamk.services;

import fr.unice.polytech.isa.teamk.EventFinder;
import fr.unice.polytech.isa.teamk.EventRegister;
import fr.unice.polytech.isa.teamk.OrganizerFinder;
import fr.unice.polytech.isa.teamk.ProviderFinder;
import fr.unice.polytech.isa.teamk.entities.Event;
import fr.unice.polytech.isa.teamk.entities.EventStatus;
import fr.unice.polytech.isa.teamk.entities.Invoice;
import fr.unice.polytech.isa.teamk.entities.Provider;
import fr.unice.polytech.isa.teamk.exceptions.AlreadyExistingEventException;
import fr.unice.polytech.isa.teamk.exceptions.ExternalPartnerException;
import fr.unice.polytech.isa.teamk.exceptions.RegisterEventException;
import fr.unice.polytech.isa.teamk.exceptions.UnknownEventException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Stateless(name = "EventWS")
@WebService(targetNamespace = "http://www.polytech.unice.fr/si/4a/isa/event")
public class EventServiceImpl implements EventService {

    @EJB
    private EventRegister eventRegister;

    @EJB
    private EventFinder eventFinder;

    @EJB
    private OrganizerFinder organizerFinder;

    @EJB
    private ProviderFinder providerFinder;

    public EventServiceImpl() {
        // Mandatory Constructor
    }

    @Override
    public List<Provider> getProviders() {
        return providerFinder.searchProviders();
    }

    @Override
    public List<String> getProvidersTypes() {
        return providerFinder.retrieveProviderType();
    }

    @Override
    public void submitNewEvent(String eventName, String startDate, String endDate, int nbAttendee, List<String> providers, String organizerEmail) throws AlreadyExistingEventException {
        eventRegister.submitNewEvent(eventName, startDate, endDate, nbAttendee, providers, organizerEmail);
    }

    @Override
    public List<Event> getSubmittedEvents() {
        return eventFinder.searchEventByStatus(EventStatus.SUBMITTED);
    }

    @Override
    public HashMap<String, EventStatus> getEventsStatusByOrganizer(String organizerEmail) {
        Set<Event> eventSet = eventFinder.searchEventByOrganizer(organizerEmail);
        HashMap<String, EventStatus> events = new HashMap<>();

        for (Event event : eventSet) {
            events.put(event.getName(), event.getStatus());
        }

        return events;
    }

    @Override
    public HashMap<String, Invoice> getEventsInvoiceByOrganizer(String organizerEmail) {
        Set<Event> eventSet = eventFinder.searchEventByOrganizer(organizerEmail);
        HashMap<String, Invoice> events = new HashMap<>();

        for (Event event : eventSet) {
            events.put(event.getName(), event.getInvoice());
        }

        return events;
    }

    @Override
    public boolean confirmEvent(String eventName, List<String> rooms) throws RegisterEventException, UnknownEventException, ExternalPartnerException {
        return eventRegister.confirmEvent(eventName, rooms);
    }

}

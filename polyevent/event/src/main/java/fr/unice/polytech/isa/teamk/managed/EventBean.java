package fr.unice.polytech.isa.teamk.managed;

import fr.unice.polytech.isa.teamk.EventFinder;
import fr.unice.polytech.isa.teamk.EventRegister;
import fr.unice.polytech.isa.teamk.OrganizerFinder;
import fr.unice.polytech.isa.teamk.ProviderFinder;
import fr.unice.polytech.isa.teamk.entities.Event;
import fr.unice.polytech.isa.teamk.entities.Organizer;
import fr.unice.polytech.isa.teamk.entities.Provider;
import fr.unice.polytech.isa.teamk.exceptions.AlreadyExistingEventException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@ManagedBean
@SessionScoped
public class EventBean implements Serializable {

    private static final Logger log = Logger.getLogger(EventBean.class.getName());

    @EJB
    private transient EventRegister eventRegister;
    @EJB
    private transient EventFinder eventFinder;
    @EJB
    private transient OrganizerFinder organizerFinder;

    @EJB
    private transient ProviderFinder providerFinder;

    @ManagedProperty("#{organizerBean.email}")
    private String email;

    private String name;
    private String startDate;
    private String endDate;
    private int nbAttendee;
    private List<Provider> providers;
    private List<String> selProviders;

    private String eventId;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    private Organizer organizer;

    public EventBean() {
        // Mandatory Bean Constructor
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getNbAttendee() {
        return nbAttendee;
    }

    public void setNbAttendee(int nbAttendee) {
        this.nbAttendee = nbAttendee;
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }

    public List<String> getSelProviders() {
        return selProviders;
    }

    public void setSelProviders(List<String> selProviders) {
        this.selProviders = selProviders;
    }

    public Organizer getOrganizer() {
        return organizer;
    }

    public void setOrganizer(Organizer organizer) {
        this.organizer = organizer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @PostConstruct
    private void initialize() {
        organizerFinder.searchOrganizerByEmail(email).ifPresent(organizer -> this.organizer = organizer);
        providers = providerFinder.searchProviders();
//        log.log(Level.WARNING, "ORGANIZER 888888888 ---- " + eventFinder.searchEventByOrganizer(email));
    }

    public String registerEvent() {
       /* Event event = new Event(name, Timestamp.valueOf(startDate), Timestamp.valueOf(endDate), nbAttendee);
        manager.persist(event);*/

        log.log(Level.INFO, "SELPROVIDESR ---------- " + selProviders);

        try {
            setEventId(eventRegister.submitNewEvent(name, startDate, endDate, nbAttendee, selProviders, email));
            return Signal.ADDED;
        } catch (AlreadyExistingEventException e) {
            log.log(Level.WARNING, "Event Already Existing error", e);
            FacesContext.getCurrentInstance().addMessage("event-error", new FacesMessage("Event Already Existing!"));
            return Signal.EXISTING;
        }

    }


    /***************************
     ** Virtual Data bindings **
     ***************************/

    public List<Event> getEvents() {
        return new ArrayList<>(eventFinder.searchEventByOrganizer(getOrganizer().getEmail()));
//        return new ArrayList<>();
    }


}

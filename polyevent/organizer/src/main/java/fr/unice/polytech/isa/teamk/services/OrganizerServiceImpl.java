package fr.unice.polytech.isa.teamk.services;

import fr.unice.polytech.isa.teamk.OrganizerFinder;
import fr.unice.polytech.isa.teamk.OrganizerRegister;
import fr.unice.polytech.isa.teamk.Payment;
import fr.unice.polytech.isa.teamk.exceptions.AlreadyExistingOrganizerException;
import fr.unice.polytech.isa.teamk.exceptions.AlreadyLoggedInOrganizerException;
import fr.unice.polytech.isa.teamk.exceptions.ExternalPartnerException;
import fr.unice.polytech.isa.teamk.exceptions.UnknownOrganizerException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

@Stateless(name = "OrganizerWS")
@WebService(targetNamespace = "http://www.polytech.unice.fr/si/4a/isa/organizer")
public class OrganizerServiceImpl implements OrganizerService {

    @EJB
    private Payment payment;

    @EJB
    private OrganizerRegister organizerRegister;

    @EJB
    private OrganizerFinder organizerFinder;

    public OrganizerServiceImpl() {
        // Mandatory Constructor
    }

    @Override
    public void registerOrganizer(String name, String email, String password, String phone) throws AlreadyExistingOrganizerException {
        organizerRegister.registerOrganizer(name, email, password, phone);
    }

    @Override
    public void loginOrganizer(String email, String password) throws UnknownOrganizerException, AlreadyLoggedInOrganizerException {
        organizerRegister.loginOrganizer(email, password);
    }

    @Override
    public void disconnectOrganizer(String email) {
        organizerRegister.disconnectOrganizer(email);
    }

    @Override
    public void sendPayment(String email, String eventName, String creditCard) throws ExternalPartnerException {
        payment.pay(organizerFinder.searchOrganizerByEmail(email).get(), eventName, creditCard);
    }

}

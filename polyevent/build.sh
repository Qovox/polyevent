#!/bin/bash

# Build war
mvn clean package install

# Build backend_services docker image
docker build -t backend_services event/

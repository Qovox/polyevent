package fr.unice.polytech.isa.teamk.components;

import fr.unice.polytech.isa.teamk.entities.Provider;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.logging.Level;
import java.util.logging.Logger;

@Startup
@Singleton
public class StartupBean {

    public enum States {BEFORESTARTED, STARTED, PAUSED, SHUTTINGDOWN}

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    public static final String DEFAULT_ADDRESS = "192.168.99.100";

    @PersistenceContext
    private EntityManager manager;

    private States state;

    @PostConstruct
    public void initialize() {
        state = States.BEFORESTARTED;
        initProviders();
        state = States.STARTED;
        log.log(Level.INFO, "Service Started");
    }

    public States getState() {
        return state;
    }

    public void setState(States state) {
        this.state = state;
    }

    private void initProviders() {
        CriteriaBuilder builder = manager.getCriteriaBuilder();
        CriteriaQuery<Provider> criteria = builder.createQuery(Provider.class);
        Root<Provider> root = criteria.from(Provider.class);
        criteria.select(root);
        TypedQuery<Provider> query = manager.createQuery(criteria);

        if (query.getResultList().isEmpty()) {
            log.log(Level.INFO, "Providers Initialization");

            manager.persist(new Provider("Jack", "OSS117", "4545", "lol@yopmail.fr", "Comique", 42));
            manager.persist(new Provider("Gandalf", "LOTR", "0000", "lotr@yopmail.fr", "Mage", 500));
            manager.persist(new Provider("Merlin", "Kamelott", "2222", "k@yopmail.fr", "Mage", 20));
            manager.persist(new Provider("Tom Riddles", "Poudlard", "6666", "voldi@yopmail.fr", "Mage", 4));
            manager.persist(new Provider("Obi-Wan Kenobi", "Coruscant", "7777", "ben@yopmail.fr", "Jedi", 1000));
        }
    }

}

package fr.unice.polytech.isa.teamk.entities;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class Invoice implements Serializable {

    private float price;

    private String description;

    public Invoice() {
        // Mandatory Bean Constructor
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return Float.compare(invoice.getPrice(), getPrice()) == 0 &&
                Objects.equals(getDescription(), invoice.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrice(), getDescription());
    }

}

package fr.unice.polytech.isa.teamk.entities;

public enum RoomType {

    AMPHI(100), COURS(50), TD(25), UNKNOWN(0);

    private float price;

    RoomType(float price) {
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    public static RoomType convert(int value) {
        switch (value) {
            case 0:
                return RoomType.AMPHI;
            case 1:
                return RoomType.COURS;
            case 2:
                return RoomType.TD;
            default:
                return RoomType.UNKNOWN;
        }
    }

}

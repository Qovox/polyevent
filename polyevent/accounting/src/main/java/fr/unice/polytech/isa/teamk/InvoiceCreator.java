package fr.unice.polytech.isa.teamk;

import fr.unice.polytech.isa.teamk.entities.Event;
import fr.unice.polytech.isa.teamk.entities.Invoice;

import javax.ejb.Local;

@Local
public interface InvoiceCreator {

    Invoice createInvoice(Event event);

}

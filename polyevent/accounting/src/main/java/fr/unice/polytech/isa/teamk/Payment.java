package fr.unice.polytech.isa.teamk;

import fr.unice.polytech.isa.teamk.entities.Organizer;
import fr.unice.polytech.isa.teamk.exceptions.ExternalPartnerException;

import javax.ejb.Local;

@Local
public interface Payment {

    void pay(Organizer organizer, String event, String creditCard) throws ExternalPartnerException;

}

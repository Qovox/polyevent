package fr.unice.polytech.isa.teamk.async;

import org.apache.openejb.util.LogCategory;
import org.apache.openejb.util.Logger;

import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven
public class PrinterAck implements MessageListener {

    private static final Logger log = Logger.getInstance(LogCategory.ACTIVEMQ, PrinterAck.class);

    @Override
    public void onMessage(Message message) {
        try {
            String data = ((TextMessage) message).getText();
            log.info("\n\n****\n** ACK: " + data + "\n****\n");
        } catch (JMSException e) {
            log.error(e.getMessage(), e);
        }
    }

}

package fr.unice.polytech.isa.teamk.external;

import fr.unice.polytech.isa.teamk.exceptions.ExternalPartnerException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.json.JSONObject;

import javax.ws.rs.core.MediaType;
import java.util.logging.Level;
import java.util.logging.Logger;

import static fr.unice.polytech.isa.teamk.components.StartupBean.DEFAULT_ADDRESS;

public class PaymentService {

    private String url;

    private static final Logger log = Logger.getLogger(Logger.class.getName());

    public PaymentService(String host, String port) {
        this.url = "http://" + host + ":" + port + "/payment";
    }

    public PaymentService() {
        this(DEFAULT_ADDRESS, "9090");
    }

    public boolean performPayment(String creditCard, double value) throws ExternalPartnerException {
        // Building payment request
        JSONObject request = new JSONObject().put("CreditCard", creditCard).put("Amount", value);

        // Sending a Payment request to the mailbox
        Integer id;
        try {
            String str = WebClient.create(url).path("/mailbox")
                    .accept(MediaType.APPLICATION_JSON_TYPE).header("Content-Type", MediaType.APPLICATION_JSON)
                    .post(request.toString(), String.class);
            id = Integer.parseInt(str);

            log.log(Level.SEVERE, "Payment sent: {0}\nAnswer: {1}", new Object[]{request.toString(), str});
        } catch (Exception e) {
            throw new ExternalPartnerException(url + "/mailbox", e);
        }

        // Retrieving the payment status
        JSONObject payment;
        try {
            String response = WebClient.create(url).path("/payments/" + id).get(String.class);
            payment = new JSONObject(response);

            log.log(Level.SEVERE, "Payment details: {0}\n", new Object[]{payment.toString()});
        } catch (Exception e) {
            throw new ExternalPartnerException(url + "payments/" + id, e);
        }
        // Assessing the payment status
        return (payment.getInt("Status") == 0);
    }

}

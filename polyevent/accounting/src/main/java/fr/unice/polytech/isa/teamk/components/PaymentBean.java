package fr.unice.polytech.isa.teamk.components;

import fr.unice.polytech.isa.teamk.Payment;
import fr.unice.polytech.isa.teamk.entities.Event;
import fr.unice.polytech.isa.teamk.entities.EventStatus;
import fr.unice.polytech.isa.teamk.entities.Organizer;
import fr.unice.polytech.isa.teamk.exceptions.ExternalPartnerException;
import fr.unice.polytech.isa.teamk.external.PaymentService;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class PaymentBean implements Payment {

    private static final Logger log = Logger.getLogger(AccountancyBean.class.getName());

    private PaymentService paymentService;

    public PaymentBean() {
        // Mandatory Bean Constructor
    }

    @Override
    public void pay(Organizer organizer, String event, String creditCard) throws ExternalPartnerException {
        for (Event e : organizer.getEvents()) {
            if (e.getName().equals(event)) {
                paymentService.performPayment(creditCard, e.getInvoice().getPrice());
                Optional<EventStatus> next = EventStatus.next(e.getStatus());

                if (next.isPresent()) {
                    log.log(Level.SEVERE, "Moving event [" + e.getId() + ", " + e.getName() + "] to next step");
                    e.setStatus(next.get());
                }
            }
        }
    }

    @PostConstruct
    private void initializeRestPartnership() throws ExternalPartnerException {
        try {
            Properties prop = new Properties();
            prop.load(this.getClass().getResourceAsStream("/payment.properties"));
            paymentService = new PaymentService(prop.getProperty("paymentHostName"),
                    prop.getProperty("paymentPortNumber"));
        } catch (IOException e) {
            log.log(Level.INFO, "Cannot read payment.properties file", e);
            throw new ExternalPartnerException("Cannot read payment.properties file", e);
        }
    }

}

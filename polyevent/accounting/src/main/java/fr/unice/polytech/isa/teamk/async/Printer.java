package fr.unice.polytech.isa.teamk.async;

import fr.unice.polytech.isa.teamk.entities.Event;
import org.apache.openejb.util.LogCategory;
import org.apache.openejb.util.Logger;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.*;
import javax.jms.IllegalStateException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "/queue/printer")})
public class Printer implements MessageListener {

    private static final Logger log = Logger.getInstance(LogCategory.ACTIVEMQ, Printer.class);

    @Resource
    private MessageDrivenContext context;

    @Resource
    private ConnectionFactory connectionFactory;

    @Resource(name = "PrinterAck")
    private Queue acknowledgmentQueue;

    @Override
    public void onMessage(Message message) {
        try {
            Event data = (Event) ((ObjectMessage) message).getObject();
            handle(data);
        } catch (JMSException e) {
            log.error("Java message service exception while handling " + message);
            log.error(e.getMessage(), e);
            context.setRollbackOnly();
        }
    }

    @PersistenceContext
    private EntityManager entityManager;

    private void handle(Event data) throws IllegalStateException {
        data = entityManager.merge(data);
        try {
            log.info("Printer:\n  Printing invoice for event" + data.getName() + " #" + data.getId());
            Thread.sleep(4000);
            log.info("\n    " + data.getInvoice().getDescription());
            log.info("\n  done!");
            respond(data.getId());
        } catch (InterruptedException | JMSException e) {
            log.error(e.getMessage(), e);
            throw new IllegalStateException(e.toString());
        }
    }

    private void respond(int invoiceId) throws JMSException {
        Connection connection = null;
        Session session = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(acknowledgmentQueue);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            producer.send(session.createTextMessage(invoiceId + ";PRINTED"));
        } finally {
            if (session != null)
                session.close();
            if (connection != null)
                connection.close();
        }
    }

}

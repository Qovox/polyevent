package fr.unice.polytech.isa.teamk.components;

import fr.unice.polytech.isa.teamk.InvoiceCreator;
import fr.unice.polytech.isa.teamk.entities.Event;
import fr.unice.polytech.isa.teamk.entities.Invoice;
import fr.unice.polytech.isa.teamk.entities.Provider;
import fr.unice.polytech.isa.teamk.entities.Room;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class AccountancyBean implements InvoiceCreator {

    private static final Logger log = Logger.getLogger(AccountancyBean.class.getName());

    @Resource
    private ConnectionFactory connectionFactory;

    @Resource(name = "Printer")
    private Queue printerQueue;

    public AccountancyBean() {
        // Mandatory Bean Constructor
    }

    @Override
    public Invoice createInvoice(Event event) {
        Invoice invoice = new Invoice();

        float price = 0;

        StringBuilder description = new StringBuilder(
                "*-------------- Invoice --------------*\n" +
                        "*------------- Polyevent -------------*\n");

        for (Provider provider : event.getProviders()) {
            price += provider.getPrice();
            description.append("Provider : ").append(provider.getName()).append("\nPrice : ")
                    .append(provider.getPrice()).append("\n");
        }

        if (event.getOrganizer().isExternal()) {
            for (Room room : event.getRooms()) {
                price += room.getType().getPrice();
            }
        }

        invoice.setDescription(description.toString());
        invoice.setPrice(price);

        printInvoice(event);

        return invoice;
    }

    private void printInvoice(Event event) {
        try (Connection connection = connectionFactory.createConnection();
             Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
             MessageProducer printer = session.createProducer(printerQueue)) {
            connection.start();
            printer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            printer.send(session.createObjectMessage(event));
        } catch (JMSException e) {
            log.log(Level.WARNING, "Unable to send Invoice in event [" + event.getId() + "] to printer", e);
        }
    }

}

#!/bin/bash

# Compile C# code
csc src/*.cs -out:server.exe

# Build ext_services docker image
docker build -t ext_services .

# Archi

## Structure du projet

* Un client java (CLI)
* Coeur du métier et logique système en J2EE (Communication avec le client en SOAP)
* Des services externes implémentés en .NET (Communication avec J2EE en REST)

## Points forts

+ Bonne séparation des modules. Si une partie tombe en panne par exemple organizer, les responsables peuvent toujours travailler et inversement
+ Composants réutilisables
+ Extensibilité du logiciel grâce à SOA
+ Intercepteur pour vérifier les dates
+ Layered pattern : une couche peut être modifiée sans affecter celle au dessus
+ JSF pour pouvoir enregistrer un organizer et des events
+ Exceptions personnalisées. La quasi-totalité des méthodes dans les différents webservices peuvent générer une exception personnalisée.

## Points faibles

- Base de données dans un seul module (Fort couplage à ce module). Peut être aussi un avantage dans le cas où un module tombe en panne, on a toujours accès aux données
- Intercepteurs non exploités à leur plein potentiel
- Fort couplage à JSON pour la communication avec les services externes
- Testabilité mauvaise (besoin de database et de mocks)
- Futur potentiel god component (Component Event)
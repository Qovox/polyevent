package cli.commands.organizer;

import api.PEPublicAPI;
import cli.framework.Command;

import java.util.List;

public class PayInvoice extends Command<PEPublicAPI> {

    private String eventName;
    private String creditCard;

    @Override
    public String identifier() {
        return "pay_invoice";
    }

    @Override
    public void load(List<String> args) {
        short argIndex = 0;

        eventName = args.get(argIndex++);
        creditCard = args.get(argIndex);
    }

    @Override
    public void execute() throws Exception {
        if (!LoginOrganizer.loggedInOrganizerId.isEmpty()) {
            shell.system.organizerService.sendPayment(LoginOrganizer.loggedInOrganizerId, eventName, creditCard);
        } else {
            System.err.println("You have to login to invoke this command.");
        }
    }

    @Override
    public String describe() {
        return "Pay the invoice of a given event. Magic Key in credit card (896983)\n" +
                "	--> pay_invoice <Event Name> <Credit Card>";
    }

}

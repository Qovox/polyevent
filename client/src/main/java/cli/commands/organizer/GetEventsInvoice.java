package cli.commands.organizer;

import api.PEPublicAPI;
import cli.framework.Command;
import stubs.eventstubs.GetEventsInvoiceByOrganizerResponse;

import java.util.List;

public class GetEventsInvoice extends Command<PEPublicAPI> {

    @Override
    public String identifier() {
        return "get_events_invoice";
    }

    @Override
    public void load(List<String> args) {

    }

    @Override
    public void execute() throws Exception {
        if (!LoginOrganizer.loggedInOrganizerId.isEmpty()) {
            List<GetEventsInvoiceByOrganizerResponse.Return.Entry> submittedEvents = shell.system.eventService.
                    getEventsInvoiceByOrganizer(LoginOrganizer.loggedInOrganizerId).getEntry();
            System.out.println("List of events with their invoice:");
            int i = 1;
            for (GetEventsInvoiceByOrganizerResponse.Return.Entry e : submittedEvents) {
                System.out.println(i++ + ". " + e.getKey() + "\n" + e.getValue().getDescription()
                        + "Total Price: " + e.getValue().getPrice());
            }
        } else {
            System.err.println("You have to login to invoke this command.");
        }
    }

    @Override
    public String describe() {
        return "Retrieves all the events invoice of a given organizer from the Polyevent system\n" +
                "	--> get_events_invoice";
    }

}

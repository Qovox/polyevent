package cli.commands.organizer;

import api.PEPublicAPI;
import cli.framework.Command;
import stubs.eventstubs.GetEventsStatusByOrganizerResponse;

import java.util.List;

public class GetEventsStatus extends Command<PEPublicAPI> {

    @Override
    public String identifier() {
        return "get_events_status";
    }

    @Override
    public void load(List<String> args) {

    }

    @Override
    public void execute() throws Exception {
        if (!LoginOrganizer.loggedInOrganizerId.isEmpty()) {
            List<GetEventsStatusByOrganizerResponse.Return.Entry> submittedEvents = shell.system.eventService.
                    getEventsStatusByOrganizer(LoginOrganizer.loggedInOrganizerId).getEntry();
            System.out.println("List of events with their status:");
            int i = 1;
            for (GetEventsStatusByOrganizerResponse.Return.Entry e : submittedEvents) {
                System.out.println(i++ + ". " + e.getKey() + ", Status: " + e.getValue().name());
            }
        } else {
            System.err.println("You have to login to invoke this command.");
        }
    }

    @Override
    public String describe() {
        return "Retrieves all the events status of a given organizer from the Polyevent system\n" +
                "	--> get_events_status";
    }

}

package cli.commands.organizer;

import api.PEPublicAPI;
import cli.framework.Command;
import stubs.eventstubs.Provider;

import java.util.List;

public class GetProviders extends Command<PEPublicAPI> {

    @Override
    public String identifier() {
        return "get_providers";
    }

    @Override
    public void execute() throws Exception {
        if (!LoginOrganizer.loggedInOrganizerId.isEmpty()) {
            List<Provider> providers = shell.system.eventService.getProviders();
            System.out.println("List of the different providers :");
            int i = 1;

            for (Provider p : providers) {
                System.out.println(i++ + ". " + p.getName() + " (ID: " + p.getId() + ")");
            }
        } else {
            System.err.println("You have to login to invoke this command.");
        }
    }

    @Override
    public String describe() {
        return "Retrieves all the providers available from the Polyevent system\n" +
                "	--> get_providers";
    }

}

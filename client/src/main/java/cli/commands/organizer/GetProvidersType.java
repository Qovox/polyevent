package cli.commands.organizer;

import api.PEPublicAPI;
import cli.framework.Command;

import java.util.List;

public class GetProvidersType extends Command<PEPublicAPI> {

    @Override
    public String identifier() {
        return "get_providers_type";
    }

    @Override
    public void execute() throws Exception {

        if (!LoginOrganizer.loggedInOrganizerId.isEmpty()) {
            List<String> providers = shell.system.eventService.getProvidersTypes();
            System.out.println("List of the different providers :");
            int i = 1;
            for (String p : providers) {
                System.out.println(i++ + ". " + p);
            }
        } else {
            System.err.println("You have to login to invoke this command.");
        }

    }

    @Override
    public String describe() {
        return "Retrieves all the providers available from the Polyevent system\n" +
                "	--> get_providers_type";
    }

}

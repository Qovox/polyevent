#!/bin/bash

echo -e "Le fichier zip jenkins_blue_ocean.zip à la racine du projet (dossier main/) contient le dossier jobs et le fichier config.xml.\n"
echo -e "Voici la commande docker à exécuter pour lancer Jenkins : (où <Your_Jenkins_Home> doit-être remplacé par le chemin vers votre jenkins_home)\n"
echo -e "docker run -d -p 8080:8080 -p 50000:50000 -v <Your_Jenkins_Home>:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --name jenkins jenkinsci/blueocean\n"
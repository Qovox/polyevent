# Scenarios

## Comment fonctionne le service externe Calendar (EDT)

Nous avons un service externe nommé Calendar. Ce dernier simule un EDT. Il est fait de sorte à ce qu'il simule 
deux semaines d'emploi du temps (en simulant tous les jours de la semaine) similaires en termes de salles disponibles.

De ce fait, si nous sommes le jeudi 10 mai 2018 (10/05/2018), les jours d'emplois du temps simulés vont du 
lundi 7 mai 2018 (07/05/2018) au dimanche 20 mai 2018 (20/05/2018). 

Au moment où nous seront le lundi d'après, à savoir le lundi 14 mai 2018 (14/05/2018), les jours mockés 
iront du lundi 14 mai 2018 (14/05/2018) au dimanche 27 mai 2018 (27/05/2018).

## Scenarios 1

Rentrez les commandes suivantes dans la console : 

```
# Registering and logging in a given organizer (External)
register_organizer LARA jeremy.lara@wanadoo.fr pwd 0699887766
login_organizer jeremy.lara@wanadoo.fr pwd

# Getting available providers information
get_providers
```

**Pensez appuyez sur entrée pour valider la dernière commande après le copié/collé.** (Les commandes dans la consoles sont validées par un retour chariot)

Ensuite il faut soumettre un évènement.
Là il faut être vigilent et tenir compte de ce qui a été expliqué dans la partie **Comment fonctionne le service externe Calendar (EDT)**.
Voici un exemple de comment soumettre un évènement si nous étions le par exemple le mercredi 9 mai 2018 (09/05/2018) :

```
submit_event PolyDating 8 11 5 2018 15 11 5 2018 100 1 2
```

Cette commande soumet l'évènement nommé PolyDating, commençant à 8 heures du matin le 11/5/2018 et se terminant à 15 le 11/5/2018.
Il y aura 100 personnes présentes et les providers d'ID 1 et 2 seront requis.

**Il ne faudra modifier que la date dans cette commande**. Il suffit d'adapter la date de début et de fin, sans changer
l'heure de début ni celle de fin de l'évènement cependant. Pour que la date soit valide et que l'évènement soit ainsi correctement soumis
au système polyevent :
+ Ne pas mettre de 0 inutiles (_Exemple:_ 5 au lieu de 05 pour spécifier le mois de mai).
+ Il faut que la date de début soit inférieure à la date de fin.
+ Que la date de début soit après (différence d'un jour au moins) la date d'aujourd'hui (pour éviter tout problème).

Ensuite une fois l'évènement soumis copiez/collez les commandes suivantes dans la console :

```
# Checking Organizer's event status
get_events_status

# Registering and logging in a given responsible
register_responsible BOULET olivier.boulet@wanadoo.fr pwd 0655443322
login_responsible olivier.boulet@wanadoo.fr pwd

# Getting events status given an organizer
get_events_status

# Getting the submitted event list
get_submitted_events
```

La prochaine commande utilisera **les dates précédemment utilisées pour soumettre l'évènement**. Exemple d'utilisation :

```
get_vacant_rooms 8 11 5 2018 15 11 5 2018
```

Cette commande récupère auprès du service externe Calendar les salles libres à partir de 8 heures
le 11/5/2018 jusqu'à 15 heures le 11/5/2018.

Utilisez cette commande dans la console en remplaçant avec les dates précédemment utilisées.

Ensuite, copiez/collez les commandes suivantes dans la console :

```
# Confirming event to Calendar Service
confirm_event PolyDating E+132

# Getting events status given an organizer to see effective confirmation
get_events_status

# Getting events invoice given an organizer
get_events_invoice

# Pay event
pay_invoice PolyDating 1234-896983-5678-9090

# Getting events status given an organizer to see effective payment
get_events_status
```

Fin du scénario.
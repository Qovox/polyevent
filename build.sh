#!/bin/bash

## Building client
cd client
sh build.sh
cd ..

## Building the polyevent system
cd polyevent
sh build.sh
cd ..

## Building the .Net system
cd dotNet
sh build.sh
cd ..
